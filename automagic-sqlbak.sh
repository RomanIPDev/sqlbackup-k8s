PROJECT=garantyr-prod
CLUSTER=cluster-1
ZONE=europe-north1-a

gcloud container clusters get-credentials $CLUSTER --zone $ZONE --project $PROJECT

ipgarpg="$(kubectl get services gar-pg -o wide -n gar-be | grep "ClusterIP" | awk '{print $3}')"

echo "IP gar-pg in GCP: $ipgarpg"

if [ -z "$ipgarpg" ]; then {
echo "[ IP address host gar-pg in GCP not received. Try again. Exit. ]"
exit
} else {

IP_GARPG_VALUE="$ipgarpg"

template=`cat "config-sqlbak.yml" | sed "s/{{IP_GARPG_VALUE}}/$IP_GARPG_VALUE/g"`

echo "$template" | kubectl apply -f -

kubectl rollout restart deployment sqlbak -n web

}
fi
exit
